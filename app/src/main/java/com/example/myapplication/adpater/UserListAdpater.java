package com.example.myapplication.adpater;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.example.myapplication.R;
import com.example.myapplication.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.String.valueOf;

public class UserListAdpater extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String, Object>> userList;


    @RequiresApi(api = Build.VERSION_CODES.M)
    public UserListAdpater(Context context, ArrayList<HashMap<String, Object>> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return userList.get(i);

    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null);

        TextView tvname = view1.findViewById(R.id.tvLstName);
        TextView tvemail = view1.findViewById(R.id.tvLstEmail);
        TextView tvmale = view1.findViewById(R.id.tvlstMale);
        TextView tvfemale = view1.findViewById(R.id.tvlstFemale);

        tvname.setText(valueOf(userList.get(position).get(Const.FIRST_NAME)));
        tvemail.setText(valueOf(userList.get(position).get(Const.EMAIL_ADDRESS)));

        if (valueOf(userList.get(position).get(Const.GENDER)).equals("Male")) {
            tvfemale.setVisibility(View.GONE);
        } else {
            tvmale.setVisibility(View.GONE);
        }

        return view1;
    }

}
