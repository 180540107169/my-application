package com.example.myapplication;

import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.adpater.UserListAdpater;
import com.example.myapplication.util.Const;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class SecondActivity extends AppCompatActivity {


    ListView lvUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdpater userListAdpater;


    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initViewRefernce();
        bindViewValues();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    void bindViewValues() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));


        userListAdpater = new UserListAdpater(this, userList);
        lvUsers.setAdapter(userListAdpater);


        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(SecondActivity.this, userList.get(position).get(Const.GENDER).toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    void initViewRefernce() {
        lvUsers = findViewById(R.id.lvActuserList);
        getSupportActionBar().setTitle(R.string.lbl_display);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}

